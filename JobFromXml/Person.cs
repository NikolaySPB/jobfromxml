﻿using System;

namespace JobFromXml
{
    public class Person
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Speciality { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age { get; set; }

        public Person()
        {
            LastName = RandomPeople.GetLastName();
            FirstName = RandomPeople.GetFirstName();
            Age = RandomPeople.GetAge();
        }

        public Person(string lastName, string firstName, int age)
        {
            LastName = lastName;
            FirstName = firstName;
            Speciality = RandomPeople.GetRandomStatus(age);
            BirthDate = DateTime.Today.AddYears(-age);
            Age = age;
        }

        public Guid Id = RandomPeople.GetGuid();

        public override string ToString()
        {
            return $"{LastName}    \t{FirstName}    \t{Speciality}    \t{BirthDate.ToShortDateString()} \tВозраст: {Age}";
        }
    }
}
