﻿using System;
using System.Collections.Generic;

namespace JobFromXml
{
    interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }
}
