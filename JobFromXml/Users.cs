﻿using System;
using System.Linq;

namespace JobFromXml
{
    class Users : IAccountService
    {
        private readonly Repository repository;

        public Users(Repository repository)
        {
            this.repository = repository;
        }

        public void AddAccount(Account account)
        {
            DataChecking(account);
            repository.Add(account);
        }

        private void DataChecking(Account account)
        {
            var date = DateTime.Now.AddYears(-18);
            if (account.BirthDate > date)
            {
                throw new Exception("Возрастное ограничение! Доступ только от 18 лет.");
            }
            
            if (string.IsNullOrEmpty(account.LastName))
            {
                throw new Exception("Не указана фамилия.");
            }

            if (string.IsNullOrEmpty(account.FirstName))
            {
                throw new Exception("Не указано имя.");
            }

            if (repository.GetAll().Any(w =>
                w.FirstName == account.FirstName && w.LastName == account.LastName &&
                w.BirthDate == account.BirthDate))
            {
                throw new Exception("Данная учетная запись уже существует.");
            }
        }
    }
}
