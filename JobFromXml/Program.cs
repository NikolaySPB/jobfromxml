﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections.Generic;
using System.IO;

namespace JobFromXml
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Создание Xml файла с данными
            //var person = new List<Person>();
            //for (int i = 0; i <= 10; i++)
            //{
            //    person.Add(new Person(RandomPeople.GetLastName(), RandomPeople.GetFirstName(), RandomPeople.GetAge()));
            //}

            //IExtendedXmlSerializer serializer = new ConfigurationContainer().ConfigureType<Person>().Create();
            //string contents = serializer.Serialize(person);
            //File.WriteAllText("people.xml", contents);
            #endregion

            #region Десериализация Xml файла

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Десериализация Xml файла\n");
            Console.ResetColor();

            JobFromXmlReader jobFromXml;
            using (var stream = new FileStream("people.xml", FileMode.Open))
            {
                jobFromXml = new JobFromXmlReader(stream);
            }

            foreach (var item in jobFromXml)
            {
                Console.WriteLine(item);
            }
            #endregion

            #region Сортировка          

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\nСортировка по фамилии\n");
            Console.ResetColor();

            jobFromXml.SortByLastName();


            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\nСортировка по имени\n");
            Console.ResetColor();

            jobFromXml.SortByFirstName();


            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\nСортировка по профессии\n");
            Console.ResetColor();

            jobFromXml.SortBySpeciality();


            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\nСортировка по дате рождения\n");
            Console.ResetColor();

            jobFromXml.SortByBirthDate();


            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\nСортировка по возрасту\n");
            Console.ResetColor();

            jobFromXml.SortByAge();

            #endregion

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\nСериализация и десериализация через json. Создание новых личностей и проверка их по параметрам \n");
            Console.ResetColor();

            var repository = new Repository();
            var users = new Users(repository);

            var account = new Account(RandomPeople.GetLastName(), RandomPeople.GetFirstName(), RandomPeople.GetAge());
            users.AddAccount(account);

            var allUsers = repository.GetAll();

            foreach (var item in allUsers)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
    }
}
